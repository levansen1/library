/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Library;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        Book book1 = new Book("JAVA 2", "Tim");
        Book book2 = new Book("C++ Fundamentals", "Matthew");
        Book book3 = new Book("Python", "Edward");
        
        ArrayList<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
  
        Library library = new Library(books);
        
         
        ArrayList<Book> bks = library.getTotalBooksInLibrary();
        for(Book bk: bks){
            System.out.println("Title: " + bk.getTitle() + "and Author: " + bk.getAuthor());
        }
    }
}
